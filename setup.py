from distutils.core import setup

setup(
    name='pyfluidutils',
    version='2.0',
    description="Python Class API for live Fluidsynth controller.",
    author='Julien Quarré',
    url='https://github.com/pyfluid/pyfluidutils',
    packages=['pyfluidutils'],
)
