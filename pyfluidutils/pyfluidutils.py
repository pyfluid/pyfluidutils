import os, sys
import time
import argparse
import re

from pyfluidsynth3 import fluidhandle, fluidsettings, fluidaudiodriver, fluidsynth, fluidmidirouter, fluidmididriver

import shlex, subprocess

def read_config(config_path):
    config = dict()
    with open(config_path, 'r') as f:
        for l in f:
            m = re.match(' *([^\s]+) +([^\s]+)', l)
            if m:
                opt = m.group(1)
                val = m.group(2)
                try:
                    val = int(val)
                except ValueError:
                    try:
                        val = float(val)
                    except ValueError:
                        pass
                        # passing param as string
                config[opt] = val

    return config


class PyFluidLive():

    def __init__(self, conf={}, sf_dir='.', lib_path='/usr/lib/libfluidsynth'):

        self.handle = fluidhandle.FluidHandle(lib_path)
        self.settings = fluidsettings.FluidSettings(self.handle)
        for (k,v) in conf['settings'].items():
            self.settings[k] = v
        self.synth = fluidsynth.FluidSynth( self.handle, self.settings )
        self.adriver = fluidaudiodriver.FluidAudioDriver(self.handle, self.synth, self.settings)
        self.router = fluidmidirouter.FluidMidiRouter(self.handle, self.settings, self.synth)
        self.mdriver = fluidmididriver.FluidMidiDriver(self.handle, self.settings, self.router)

        for (chan, patch) in conf['patch'].items():
            sf, instr, name = patch
            self.synth.load_soundfont(os.path.join(sf_dir, sf), reload_presets=False)
            self.synth.bank_select(int(chan), int(instr.split('-')[0]))
            self.synth.program_change(int(chan), int(instr.split('-')[1]))

        # print(str(conf['connections']))
        self.search_and_connect_midi_clients(conf['connections']['i'], conf['connections']['o'])

    def __del__(self):
        # cmd = ["aconnect", "-x"]
        # subprocess.call(cmd, universal_newlines=True)

        del self.mdriver
        del self.router
        del self.adriver
        del self.synth
        del self.settings
        del self.handle

    def disconnect_midi_clients(self):
        cmd = ["aconnect", "-x"]
        subprocess.call(cmd, universal_newlines=True)

    def search_midi_clients(self, filter=None, direction='io'):
        cmd_table = {
            'io' : ["aconnect", "-l"],
            'i'  : ["aconnect", "-i"],
            'o'  : ["aconnect", "-o"],
        }
        if direction.lower() in cmd_table.keys():
            cmd = cmd_table[direction.lower()]
        else:
            cmd = cmd_table['io']
        ilist = subprocess.check_output(cmd, universal_newlines=True)
        m_clients = re.finditer('^ *(?:client) (\d+): \'(.*)\'.*', ilist, re.IGNORECASE + re.MULTILINE)
        ret_list = []
        for m in m_clients:
            if (filter is None) or (filter.lower() in m.group(2).lower()):
                # print('Matched filter ' + str(filter) + ' on group ' + str(m.groups()))
                ret_list.append( (int(m.group(1)), m.group(2)) )

        return ret_list

    def connect_midi_clients(self, i_id, o_id):
        self.i_client_id = i_id
        self.o_client_id = o_id
        cmd = ["aconnect", str(i_id), str(o_id)]
        subprocess.call(cmd, universal_newlines=True)

    def search_and_connect_midi_clients(self, i_label, o_label):
        i_list = []
        o_list = []
        for l in i_label:
            c = self.search_midi_clients(filter=l, direction='i')
            if c != []:
                i_list.extend(c)
        for l in o_label:
            c = self.search_midi_clients(filter=l, direction='o')
            if c != []:
                o_list.extend(c)


        if (len(i_list) == 1) and (len(o_list) == 1):
            print(str(i_list))
            print(str(o_list))
            self.i_client = i_list[0]
            self.o_client = o_list[0]
            self.connect_midi_clients(self.i_client[0], self.o_client[0])
            return 0

if __name__ == "__main__":
    LiveSynth = PyFluidLive()
    print(str(LiveSynth.config))
    print(str(LiveSynth.patch))

    LiveSynth.search_and_connect_midi_clients('vmpk', 'fluid')

    seq = (79, 78, 79, 74, 79, 69, 79, 67, 79, 72, 79, 76,
           79, 78, 79, 74, 79, 69, 79, 67, 79, 72, 79, 76,
           79, 78, 79, 74, 79, 72, 79, 76, 79, 78, 79, 74,
           79, 72, 79, 76, 79, 78, 79, 74, 79, 72, 79, 76,
           79, 76, 74, 71, 69, 67, 69, 67, 64, 67, 64, 62,
           64, 62, 59, 62, 59, 57, 64, 62, 59, 62, 59, 57,
           64, 62, 59, 62, 59, 57, 43)



    for note in seq:
        LiveSynth.synth.noteon(7, note, 1.0)
        time.sleep(0.1)
        LiveSynth.synth.noteoff(7, note)

    time.sleep(10)